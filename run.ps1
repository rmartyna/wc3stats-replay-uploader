Add-Type @'
using System;
using System.Net;
using System.IO;

public class Test
{
	private static string paramName = "replay";
	private static string url = "https://api.wc3stats.com/upload";

	private static void Log(string msg, string logPath) {
		string logData = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " - " + msg;
		File.AppendAllText(logPath, logData + Environment.NewLine);
	}
	
    public static void HttpUploadReplay(string replayPath, string logPath) {

        WebResponse wresp = null;
		try {
			string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
			byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

			HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
			wr.ContentType = "multipart/form-data; boundary=" + boundary;
			wr.Method = "POST";
			wr.KeepAlive = true;
			wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

			Log("Writing HTTP buffers...", logPath);
			Stream rs = wr.GetRequestStream();
			rs.Write(boundarybytes, 0, boundarybytes.Length);

			string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
			string header = string.Format(headerTemplate, paramName, replayPath, "multipart/form-data");
			byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
			rs.Write(headerbytes, 0, headerbytes.Length);

			Log("Reading file...", logPath);
			FileStream fileStream = new FileStream(replayPath, FileMode.Open, FileAccess.Read);
			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0) {
				rs.Write(buffer, 0, bytesRead);
			}
			fileStream.Close();			

			byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
			rs.Write(trailer, 0, trailer.Length);
			rs.Close();
		
			Log("Waiting for HTTP response...", logPath);
            wresp = wr.GetResponse();
            Stream stream2 = wresp.GetResponseStream();
            StreamReader reader2 = new StreamReader(stream2);
			string response = reader2.ReadToEnd();
            Log("Response: " + response, logPath);
		} catch(Exception ex) {
			Log("Error while uploading replay: " + ex.ToString(), logPath);
		} finally {
           if(wresp != null) {
                wresp.Close();
            }
        }
    }
}
'@

######################################################################################################################

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$logFile = $scriptPath + "\debug.log";
Function Log($msg) {
	$logData = "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss.fff') - " + $msg
	Add-content $logFile -value $logData
}

Function AssertNotAlreadyRunning() {
	$Proc = Get-WmiObject Win32_Process -Filter "Name='PowerShell.exe' AND CommandLine LIKE '%run.ps1%'" | Measure
	if($Proc.Count -gt 1) {
		Log("run.ps1 is already running. This process will be closed.")
		exit
	}
}

$global:quitNextRun = $false
Function AssertWarcraftStillRunning() {
	$Proc = Get-Process "Warcraft III" -ErrorAction SilentlyContinue | Measure
	if($Proc.Count -lt 1) {
		if($global:quitNextRun) {
			Log("Exiting process.")
			exit			
		} else {
			Log("WC3 not running. Script will quit on next function call.")
			$global:quitNextRun = $true
		}		
	} else {
		if($global:quitNextRun) {
			Log("WC3 was restarted before script could quit.")
			$global:quitNextRun = $false
		}		
	}
}

$replayPath = "$HOME\Documents\Warcraft III\Replay\LastReplay.w3g"
$global:oldDate = (Get-Item $replayPath).LastWriteTime
Function MaybeUploadReplay() {
	$newDate = (Get-Item $replayPath).LastWriteTime
	if($global:oldDate -eq $newDate) {
		Log("Replay not changed since last check." )
	}else {
		$global:oldDate = $newDate
		Log("Replay modified since last check. Sleeping to make sure IO buffer was written...")
		Start-Sleep -s 2
		Log("Uploading replay...")
		[Test]::HttpUploadReplay($replayPath, $logFile)
		Log("Replay uploaded.")
	}	
}

######################################################################################################################

Log("Starting script...")
AssertNotAlreadyRunning

while($true) {    
	AssertWarcraftStillRunning
    
	Log("While loop wait...")
	Start-Sleep -s 10
	
	MaybeUploadReplay
}