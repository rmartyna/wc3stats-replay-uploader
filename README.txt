#####################################################
# CONTACT
#####################################################

WC3Stats Discord server at: https://discordapp.com/invite/SdNvaXG.
Alternatively send message to Anders#6119 or krur#6158 on Discord.

#####################################################
# DEVELOPMENT
#####################################################

Development at: https://bitbucket.org/rmartyna/wc3stats-replay-uploader.

#####################################################
# INSTALLATION
#####################################################

Double click on WC3Stats.exe and follow setup instructions.
If everything goes well, you should see "WC3Stats Replay Uploader has been installed on your computer." message at the end.
Play a game and after it finishes, go to https://wc3stats.com/games to verify that your replay was uploaded.

#####################################################
# UNINSTALLATION
#####################################################

Open WC3Stats directory inside Warcraft III installation folder.
Double click on WC3Stats.exe and follow setup instructions.
If everything goes well, you should see "WC3Stats Replay Uploader has been uninstalled from your computer" message at the end.
Play a game and after it finishes, go to https://wc3stats.com/games to verify that your replay was NOT uploaded.