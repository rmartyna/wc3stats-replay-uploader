@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Display beginning message.
ECHO This is an installation file for a script which automatically uploads replays to WC3Stats website while Warcraft III is running.
ECHO For more information, contact Anders#6119 or krur#6158 on Discord.
ECHO Development at: https://bitbucket.org/rmartyna/wc3stats-replay-uploader.
ECHO WC3Stats Discord server at: https://discordapp.com/invite/SdNvaXG.
ECHO.
ECHO.

:: Check if script was run with administrative privileges.
ECHO Detecting if script was run as an Administrator...

NET SESSION >nul 2>&1
IF %ERRORLEVEL% == 0 (
    ECHO Success. Administrative permissions confirmed.
) ELSE (
    ECHO Failure. Administrative permissions required ^(Right click on file -^> Run as administrator^).
    PAUSE
    EXIT /B 1
)

:: Determine Warcraft III path
SET SCRIPT_DIR=%~dp0
SET DEFAULT_INSTALL_DIR=C:\Program Files (x86)\Warcraft III

:: Confirm that there is an exe file inside given directory.
ECHO Finding Warcraft III.exe file...

FOR %%i IN ("%~dp0..") DO SET "TMP_DIR=%%~fi"
SET WC3_EXE_64=!TMP_DIR!\x86_64\Warcraft III.exe
SET WC3_EXE_32=!TMP_DIR!\x86\Warcraft III.exe

IF EXIST !WC3_EXE_64! (
	ECHO Success. Warcraft III executable found.
	GOTO :AFTER_EXE
)

ECHO Failure. Could not find Warcraft III executable at path: !WC3_EXE_64!.
PAUSE
EXIT /B 1


:AFTER_EXE

:: Enable audit for process creation.
ECHO Enabling auditing for process creation...

auditpol /set /subcategory:"{0CCE922B-69AE-11D9-BED3-505054503030}" /success:enable
IF %ERRORLEVEL% == 0 (
    ECHO Success. Process creation auditing enabled.
) ELSE (
    ECHO Failure. Could not enable auditing process creation.
    PAUSE
    EXIT /B 1
)

:: Create scheduled task which starts PowerShell script.
ECHO Creating a new task in task scheduler...

SET TASK_NAME="WC3Stats Replay Uploader"
SET DELETE_COMMAND=SCHTASKS /Delete /TN !TASK_NAME! /F
!DELETE_COMMAND! >NUL 2>&1

SET WC3STATS_DIR=!SCRIPT_DIR:~0,-1!
SET WC3STATS_SCRIPT=!WC3STATS_DIR!\run.ps1
SET CREATE_COMMAND=SCHTASKS /Create /TN !TASK_NAME! /SC ONEVENT /EC Security /MO "*[System[Provider[@Guid='{54849625-5478-4994-A5BA-3E3B0328C30D}'] and Task = 13312 and (band(Keywords,9007199254740992)) and (EventID=4688)]] and *[EventData[Data[@Name='NewProcessName'] and ((Data='!WC3_EXE_64!') or (Data='!WC3_EXE_32!'))]]" /TR "PowerShell -NoProfile -ExecutionPolicy ByPass -WindowStyle Hidden -F \"!WC3STATS_SCRIPT!\" "
!CREATE_COMMAND! >NUL 2>&1
IF %ERRORLEVEL% == 0 (
    ECHO Success. Task scheduled successfully.
) ELSE (
    ECHO Failure. Could not schedule task.
    PAUSE
    EXIT /B 1
)

:: Display ending message.
ECHO Installing WC3Stats script finished successfully.
PAUSE
EXIT /B 0