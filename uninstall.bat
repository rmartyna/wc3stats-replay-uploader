@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Display beginning message.
ECHO This is an uninstallation file for a script which automatically uploads replays to WC3Stats website while Warcraft III is running.
ECHO For more information, contact Anders#6119 or krur#6158 on Discord.
ECHO Development at: https://bitbucket.org/rmartyna/wc3stats-replay-uploader.
ECHO WC3Stats Discord server at: https://discordapp.com/invite/SdNvaXG.
ECHO.
ECHO.

:: Check if script was run with administrative privileges.
ECHO Detecting if script was run as an Administrator...

NET SESSION >nul 2>&1
IF %ERRORLEVEL% == 0 (
    ECHO Success. Administrative permissions confirmed.
) ELSE (
    ECHO Failure. Administrative permissions required ^(Right click on file -^> Run as administrator^).
    PAUSE
    EXIT /B 1
)

:: Determine Warcraft III path
SET SCRIPT_DIR=%~dp0
SET DEFAULT_INSTALL_DIR=C:\Program Files (x86)\Warcraft III

:: Check if scheduler task exists.
ECHO Looking for WC3Stats task in task scheduler...

SET TASK_NAME="WC3Stats Replay Uploader"
SET QUERY_COMMAND=SCHTASKS /Query /FO LIST /TN !TASK_NAME!
!QUERY_COMMAND! >NUL 2>&1
IF %ERRORLEVEL% == 0 (
    ECHO Task found. Deleting found task...
) ELSE (
    ECHO Task not found.
	GOTO :END_MSG
)

:: Delete scheduled task
ECHO Deleting WC3Stats task in task scheduler...

SET DELETE_COMMAND=SCHTASKS /Delete /TN !TASK_NAME! /F
!DELETE_COMMAND! >NUL 2>&1
IF %ERRORLEVEL% == 0 (
    ECHO Success. Task deleted successfully.
) ELSE (
    ECHO Failure. Could not delete schedule task.
    PAUSE
    EXIT /B 1
)

:: Display ending message.
:END_MSG
ECHO Uninstalling WC3Stats script finished successfully.
PAUSE
EXIT /B 0