;This is an setup definition file for a script which automatically uploads replays to WC3Stats website while Warcraft III is running.
;For more information, contact Anders#6119 or krur#6158 on Discord.
;Development at: https://bitbucket.org/rmartyna/wc3stats-replay-uploader.
;WC3Stats Discord server at: https://discordapp.com/invite/SdNvaXG.

;------------------------------------

!include "MUI2.nsh"
!include "LogicLib.nsh"
!include "FileFunc.nsh"

Name "WC3Stats.com Replay Uploader"
OutFile "WC3Stats.exe"
InstallDir "$PROGRAMFILES\Warcraft III\"

InstallDirRegKey HKCU "Software\WC3Stats" ""
RequestExecutionLevel admin

!define MUI_ABORTWARNING
!define MUI_UNABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_UNFINISHPAGE_NOAUTOCLOSE

!define MUI_ICON "wc3stats.ico"

;------------------------------------

!macro UninstallExisting exitcode uninstcommand
Push `${uninstcommand}`
Call UninstallExisting
Pop ${exitcode}
!macroend
Function UninstallExisting
Exch $1 ; uninstcommand
Push $2 ; Uninstaller
Push $3 ; Len
StrCpy $3 ""
StrCpy $2 $1 1
StrCmp $2 '"' qloop sloop
sloop:
	StrCpy $2 $1 1 $3
	IntOp $3 $3 + 1
	StrCmp $2 ' ' 0 sloop
	IntOp $3 $3 - 1
	Goto run
qloop:
	StrCmp $3 "" 0 +2
	StrCpy $1 $1 "" 1 ; Remove initial quote
	IntOp $3 $3 + 1
	StrCpy $2 $1 1 $3
	StrCmp $2 '"' 0 qloop
run:
	StrCpy $2 $1 $3 ; Path to uninstaller
	StrCpy $1 161 ; ERROR_BAD_PATHNAME
	GetFullPathName $3 "$2\.." ; $InstDir
	IfFileExists "$2" 0 +4
	ExecWait '"$2" /S _?=$3' $1 ; This assumes the existing uninstaller is a NSIS uninstaller, other uninstallers don't support /S nor _?=
	IntCmp $1 0 "" +2 +2 ; Don't delete the installer if it was aborted
	Delete "$2" ; Delete the uninstaller
	RMDir "$3" ; Try to delete $InstDir
	RMDir "$3\.." ; (Optional) Try to delete the parent of $InstDir
Pop $3
Pop $2
Exch $1 ; exitcode
FunctionEnd

;------------------------------------

Var SelectedInstallDir
 
Function .onInit
	StrCpy $SelectedInstallDir "$PROGRAMFILES\Warcraft III\"

	ReadRegStr $0 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "UninstallString"
	${If} $0 != ""
	${AndIf} ${Cmd} `MessageBox MB_YESNO|MB_ICONQUESTION "Uninstall previous version?" /SD IDYES IDYES`
		!insertmacro UninstallExisting $0 $0
		${If} $0 <> 0
			MessageBox MB_YESNO|MB_ICONSTOP "Failed to uninstall, continue anyway?" /SD IDYES IDYES +2
				Abort
		${EndIf}
	${EndIf}
FunctionEnd
 
!define MUI_DIRECTORYPAGE_VARIABLE $SelectedInstallDir

;------------------------------------

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.txt"
!define MUI_DIRECTORYPAGE_TEXT_TOP "Setup will install WC3Stats.com Replay Uploader in the Warcraft III installation folder. If the folder specified below is not correct, click Browse and select another folder. Click Install to start the installation."
!define MUI_DIRECTORYPAGE_TEXT_DESTINATION "Warcraft III installation folder:"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

;------------------------------------

Section "Install"

StrCpy $INSTDIR "$SelectedInstallDir\WC3Stats"
SetOutPath "$INSTDIR"

File changelog.txt
File README.txt
File run.ps1
File install.bat
File uninstall.bat
File wc3stats.ico

nsExec::ExecToLog '"$INSTDIR\install.bat" $INSTDIR'
Pop $0
DetailPrint "Return value: $0"
${If} $0 == 0
	DetailPrint "Installation successful"
${Else}
	MessageBox MB_YESNO|MB_ICONSTOP 'Installation failed. Click "Show details" button for more information.' /SD IDYES IDYES
	Abort
${EndIf}

WriteRegStr HKCU "Software\WC3Stats" "" $INSTDIR
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "DisplayName" "WC3Stats Replay Uploader"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "UninstallString" '"$INSTDIR\uninstall.exe"'
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "DisplayIcon" '"$INSTDIR\wc3stats.ico"'
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "Publisher" "wc3stats.com"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "DisplayVersion" "1.1.2"
${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
IntFmt $0 "0x%08X" $0
WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "EstimatedSize" "$0"
WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "NoModify" 1
WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats" "NoRepair" 1
WriteUninstaller "$INSTDIR\uninstall.exe"

SectionEnd

;------------------------------------

Section "Uninstall"

nsExec::ExecToLog '"$INSTDIR\uninstall.bat" $INSTDIR'
Pop $0
DetailPrint "Return value: $0"
${If} $0 == 0
	DetailPrint "Uninstallation successful"
${Else}
	MessageBox MB_YESNO|MB_ICONSTOP 'Uninstallation failed. Click "Show details" button for more information.' /SD IDYES IDYES
	Abort
${EndIf}

Delete "$INSTDIR\uninstall.exe"

Delete "$INSTDIR\changelog.txt"
Delete "$INSTDIR\README.txt"
Delete "$INSTDIR\run.ps1"
Delete "$INSTDIR\install.bat"
Delete "$INSTDIR\uninstall.bat"
Delete "$INSTDIR\wc3stats.ico"
Delete "$INSTDIR\debug.log"

RMDir "$INSTDIR"

DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WC3Stats"
DeleteRegKey /ifempty HKCU "Software\WC3Stats"

SectionEnd